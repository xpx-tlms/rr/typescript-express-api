import app from '../src/index'
import request from 'supertest'
import { Service } from '../src/Services/PersonService'
import { Models_Public } from '../src/models/Public/Person'

describe('API GET /person/1', () => {
    it('should a person from the API', async () => {
      const response = await request(app).get('/person/1');
      expect(response.status).toBe(200);
    })
})
