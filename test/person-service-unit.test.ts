import { Service } from '../src/Services/PersonService'
import { Models_Public } from '../src/models/Public/Person'

let personService: Service.PersonService

beforeAll(() => {
    personService = new Service.PersonService()
})

describe("Person service", () => {
    it("should get person for id: 1", () => {
        const person = personService.getPerson(1)
        expect(person?.age).toBe(10)
        expect(person?.name).toBe("Alice")
    })
})

describe("Person service", () => {
    it("should return public model", () => {
        const person = personService.getPerson(1)
        expect(person).toBeInstanceOf(Models_Public.Person)
    })
})

// TODO...

