export namespace Models_Private {
    export class Person {
        public id: number
        public age: number
        public name: string
    
        constructor(id: number, name: string, age: number) {
            this.id = id
            this.name = name
            this.age = age
        }
    }
}
 