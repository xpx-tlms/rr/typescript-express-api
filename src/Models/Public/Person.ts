import { Models_Private } from '../Private/Person'

export namespace Models_Public {
    export class Person {
        public id: number
        public age: number
        public name: string
        
        constructor(person: Models_Private.Person) {
            this.id = person?.id
            this.age = person?.age
            this.name = person?.name
        }
    }
}
