// All the HTTP status codes that we use in the API.
export enum APIStatusCode {
    NotFound = 404,
    BadRequest = 400
}
