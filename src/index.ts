import express from 'express'
import { APIStatusCode } from './Enums'
import { Service } from './Services/PersonService'
import { Request, Response, NextFunction } from 'express'
import { body, param, validationResult  } from 'express-validator'

const app = express()

const personService = new Service.PersonService

// Middleware
app.use(express.json());

const myMiddleware = (req: Request, res: Response, next: NextFunction) => {
    console.log('Middleware function!')
    next()
}

//
// GET: /persons
//

app.get('/persons',
    (req: Request, res: Response) => {
        // Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(APIStatusCode.BadRequest).json({ errors: errors.array() });
        }
        // Business logic
        const result = personService.getAllPeople()
        result ? res.send(result) : res.sendStatus(APIStatusCode.BadRequest)   
})

//
// GET: /persons/:id
//

app.get('/persons/:id',
    param('id').isNumeric().withMessage('Id must be a number.'),
    (req: Request, res: Response) => {
        // Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(APIStatusCode.BadRequest).json({ errors: errors.array() });
        }
        // Business logic
        const result = personService.getPerson(Number(req.params.id))
        result ? res.send(result) : res.sendStatus(APIStatusCode.NotFound)   
})

//
// POST: /persons
//

app.post('/persons',
    body('name').isString().withMessage('Name must be a string.'),
    body('age').isNumeric().withMessage('Age must be a number.'),
    (req: Request, res: Response) => {
        // Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(APIStatusCode.BadRequest).json({ errors: errors.array() });
        }
        // Business logic
        const result = personService.addPerson(req.body)
        result ? res.send(result) : res.sendStatus(APIStatusCode.NotFound)   
})

//
// DELETE: /persons/:id
//

app.delete('/persons/:id',
    param('id').isNumeric().withMessage('Id must be a number.'),
    (req: Request, res: Response) => {
        // Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(APIStatusCode.NotFound).json({ errors: errors.array() });
        }
        // Business logic
        const result = personService.deletePerson(Number(req.params.id))
        result ? res.send({message: "ok"}) : res.sendStatus(APIStatusCode.BadRequest)   
})

//
// PUT: /persons/:id
//

app.put('/persons/:id',
    body('name').isString().withMessage('Name must be a string.'),
    body('age').isNumeric().withMessage('Age must be a number.'),
    (req: Request, res: Response) => {
        // Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(APIStatusCode.BadRequest).json({ errors: errors.array() });
        }
        // Business logic
        const result = personService.updatePerson(Number(req.params.id), req.body.name, req.body.age)
        result ? res.send(result) : res.sendStatus(APIStatusCode.BadRequest)   
})

app.listen(3000, () => {
    console.log('Server is running on port: 3000.');
})

export default app
