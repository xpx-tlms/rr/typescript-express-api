import { DataAccess } from '../Data/DataAccess'
import { IPersonService } from './IPersonService'
import { Models_Public } from '../models/Public/Person'

export namespace Service {

    export class PersonService implements IPersonService {
        
        //
        // Data members
        //

        private dataAccess: DataAccess

        //
        // Constructor
        //

        constructor() {
            this.dataAccess = new DataAccess()
        }

        //
        // Public methods
        //

        public getAllPeople(): Models_Public.Person[] {
            return this.dataAccess.getAllPeople() 
        }

        public getPerson = (personId: number) : Models_Public.Person | null => {
            const person = this.dataAccess.getPerson(personId)
            if (person) {
                return new Models_Public.Person(person)
            } else {
                return null
            }
        }

        public addPerson = (person: Models_Public.Person) : Models_Public.Person | null => {
            const newPerson = this.dataAccess.addPerson(person.name, person.age)
            if (newPerson) {
                return newPerson
            }
            else {
                return null
            }
        }

        public deletePerson(personId: number): boolean {
            return this.dataAccess.deletePerson(personId)
        }

        public updatePerson(personId: number, name: string, age: number) : Models_Public.Person | null {
            try {
                const updatedPerson = this.dataAccess.updatePerson(personId, name, age)
                return updatedPerson
            } catch (e) {
                return null
            }
        }
    }
}
