import { Models_Public } from '../models/Public/Person'

export interface IPersonService {
    getAllPeople(): Models_Public.Person[]
    getPerson(personId: number) : Models_Public.Person | null
    deletePerson(personId: number) : boolean
    updatePerson(personId: number, name: string, age: number) : Models_Public.Person | null
    addPerson(person: Models_Public.Person) : Models_Public.Person | null
}
