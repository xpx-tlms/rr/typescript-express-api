import { IPersonData } from './IPersonData'
import { Models_Private } from '../models/Private/Person'

export class DataAccess implements IPersonData {

    //
    // Data members
    //

    private peopleDB: Models_Private.Person[] = [] // Our mock database.

    //
    // Contructor
    //

    constructor() {
        this.peopleDB.push(new Models_Private.Person(1, "Alice", 10))
        this.peopleDB.push(new Models_Private.Person(2, "Bob", 20))
        this.peopleDB.push(new Models_Private.Person(3, "Charlie", 30))
    }

    //
    // Public Methods
    //

    public getPerson(personId: number) {
        const person = this.peopleDB.filter(p => p.id == personId)[0]
        return person
    }

    public getAllPeople(): Models_Private.Person[] {
        return this.peopleDB
    }

    public addPerson(name: string, age: number) : Models_Private.Person {
        const newPerson = new Models_Private.Person(this.peopleDB.length + 1, name, age)
        this.peopleDB.push(newPerson)
        return newPerson
    }

    public deletePerson(personId: number) : boolean {
        this.peopleDB = this.peopleDB.filter(p => p.id != personId)
        return true
    }

    public updatePerson(personId: number, newName: string, newAge: number) : Models_Private.Person {
        const person = this.peopleDB.find(p => p.id == personId)
        if (!person) {
            throw new Error("Person not found in database.")
        } 
        else {
            person.age = newAge
            person.name = newName
            return new Models_Private.Person(personId, newName, newAge)
        }
    }
}
