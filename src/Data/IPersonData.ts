import { Models_Private } from '../models/Private/Person'

export interface IPersonData {
    deletePerson(personId: number) : boolean
    addPerson(name: string, age: number) : Models_Private.Person
    getPerson(personId: number) : Models_Private.Person
    getAllPeople() : Models_Private.Person[]
    updatePerson(personId: number, name: string, age: number) : Models_Private.Person
}
