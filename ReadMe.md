# TypeScript Express API
Simple Express API using TypeScript.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Launch TypeScript compiler: `tsc --watch`
- Run the API: `nodemon dist/index.js`
- Run tests: `npm test`
- Call API: `GET` `http://localhost:3000/persons`

# Archictecture
![](./docs/typescript-api-architecture.jpg)

# Notes
Postman collection is [here](./docs/express-api.postman_collection.json).
